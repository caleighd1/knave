# Fork!
This is a fork of [mouse-knave](https://gitlab.com/Aidymouse/knave-foundryvtt/) to add support for Foundry version 10 as well as some extra features

# Knave
This is a system for Foundry Virtual Tabletop (https://foundryvtt.com/) that emulates [Ben Milton's fabulous classless old-school RPG Knave](https://www.drivethrurpg.com/product/250888/Knave).

It features an automatic character generator, an assortment of well known AD&D Monsters and a full set of icons for the games tools and gear.

### Installation
To install the Knave system, copy and paste this link [https://gitlab.com/caleighd1/knave/-/raw/main/system.json] into the Manifest URL section of the Install System dialog in the Foundry client.

# Features

![Player sheet](img/previews/player.png "Player sheet")

## Rollables

* ability and armor bonuses
* morale
* group initiative
* rest hit die
* safe haven rest
* dice tray

## Automatic Character Generator
Quickly and easily generate new characters on the fly at the click of a button.

* Rolls stats
* Creates roll-table supported description
* Sets default image
* Adds random gear from roll tables

## Level Up

* configurable xp deduction and hitpoint maximum re-roll

## Gear

Drag and drop from included compendiums


## Monster Cards
Easily manage and create monsters with simple card based design.

![Monster Card](img/previews/gnomecard.png "Monster Card")
![Monster Attacks](img/previews/attacks.png "Monster Attacks displayed in chat")

## In Built Bestiary
A selection of AD&D's most iconic monsters built into the system itself.

![Bestiary](img/previews/bestiary.png "Bestiary")

## Plans
* Add weapon and targetting support
* Expand the gear compendium with weapons
* Add compendium for spell books
* Expand the bestiary

## Thanks
Special thanks to Aidan Habedank and Rabid Baboon!