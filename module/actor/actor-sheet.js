/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class knaveActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    let w = 800
    let h = 625

    return mergeObject(super.defaultOptions, {
      classes: ["mouses-knave", "sheet", "actor"],
      template: "systems/mouses-knave/templates/actor/actor-sheet.html",
      width: w,
      height: h,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /* -------------------------------------------- */

  /** 

  /**
   * Change the actor sheet finding system to support multiple types
   * 
   * @override
   */
  get template() {
    const path = "systems/mouses-knave/templates/actor";
    return `${path}/${this.actor.type}-sheet.html`;
  }

  /** @override */
  getData() {
    const context = super.getData();
    /* data.dtypes = ["String", "Number", "Boolean"];
    for (let attr of Object.values(data.data.attributes)) {
      attr.isCheckbox = attr.dtype === "Boolean";
    } */

    // Use a safe clone of the actor data for further operations.
    const actorData = this.actor.toObject(false);

    // Add the actor's data to context.data for easier access, as well as flags.
    context.system = actorData.system;
    context.flags = actorData.flags;

    // Prepare character data and items.
    if (context.actor.type == 'character') {
      this._prepareCharacterItems(context);
      this._prepareCharacterData(context);
    }

    // Prepare NPC items.
    if (actorData.type == 'npc') {
      this._prepareCharacterItems(context);
    }

    // // Add roll data for TinyMCE editors.
    // context.rollData = context.actor.getRollData();

    // // Prepare active effects
    // context.effects = prepareActiveEffectCategories(this.actor.effects);

    return context;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} context The sheet data.
   *
   * @return {undefined}
   */
  _prepareCharacterData(context) {
    console.log("prepare character")
    console.log(context)

    // Loop through ability scores, and add their modifiers to our sheet output.
    for (let [key, ability] of Object.entries(context.system.abilities)) {
      ability.bonus = ability.defense - 10;
    }
    context.system.attributes.armor.bonus = context.system.attributes.armor.defense - 10;
    console.log(context)
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} context The sheet data.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(context) {
    console.log("prepare character items")
    console.log(context)

    let usedSlots = 0;
    const gear = [];
    const spells = [];
    // Iterate through items, allocating to containers
    for (let i of context.items) {
      i.img = i.img || DEFAULT_TOKEN;
      let s = Number(i.system.slotstaken)
      usedSlots += s
      //console.log("usedSlots (-"+i.name+"): "+s)
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to spells.
      else if (i.type === 'spell') {
        if (i.system.spellLevel <= context.system.attributes.level) {
          spells.push(i);
        }
      }
    }
    
    // Add weight from coins
    if (game.settings.get("mouses-knave", "coinsHaveWeight")) {
      let totalCoins = context.system.coins.copper + context.system.coins.silver + context.system.coins.gold + context.system.coins.platinum
      let coinSlots = (totalCoins - (totalCoins % 25)) / 100
      usedSlots += coinSlots
      //console.log("usedSlots (-coins): "+coinSlots)
      //gear.push(TODO coin gear filler)
    }
    //console.log("usedSlots = "+usedSlots)
    //console.log("maximum = "+context.system.abilities.con.defense)
    context.system.attributes.slotsremaining = context.system.abilities.con.defense - usedSlots;
    //console.log("remaining = "+context.system.attributes.slotsremaining)
    context.gear = gear;
    context.spells = spells;
    
  }

  /* -------------------------------------------- */

  /** @override */
  async activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      let context = this.getData();
      //console.log(context.actor.items)

      const li = $(ev.currentTarget);

      //const li = $(ev.currentTarget).parents(".item");
      let item = context.actor.items.get(li.data("item-id"));
      //console.log(item);
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      ev.stopPropagation(); // This makes it so we can delete and item without it popping up

      let context = this.getData();
      //console.log(context.actor.items)

      
      const li = $(ev.currentTarget).parents(".item");
      let item = context.actor.items.get(li.data("item-id"));
      item.delete()
      li.slideUp(200, () => this.render(false));
    });

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));

    // Steal Item Dragging Code
    //let handler = ev => this._onDragItemStart(ev);
    //html.find('.item-row').each((index, itemRow) => {
    //  if (itemRow.classList.contains("item-head-row")) return;
    //  itemRow.setAttribute("draggable", true);
    //  itemRow.addEventListener("dragstart", handler, false);
    //});


    /* AUTOMATIC GENERATION */
    html.find('.generate-character').click(this._onGenerateCharacter.bind(this));

    html.find('.level-up').click(this._onLevelUp.bind(this));
  }

  /**
     * Handle generating a new character
     * @param {Event} event   The originating click event
     * @private
     */
  async _onGenerateCharacter(event) {
    // Find the pack with all the rollable tables in it
    const tablePack = game.packs.get("mouses-knave.character-generation-tables");
    
    let context = this.getData();

    await this._clearCharacter(context);
    await this._generateStats(context);
    await this._generateDescription(context, tablePack);
    await this._generateArmor(context, tablePack);
    await this._generateGear(context, tablePack);
    
  }

  /**
     * Handle leveling
     * @param {Event} event   The originating click event
     * @private
     */
  async _onLevelUp(event) {
    let context = this.getData();
    let targetXp = game.settings.get("mouses-knave", "levelUpExperience");
    let currentXp = context.system.attributes.exp.value;
    let currentLevel = parseInt(context.system.attributes.level.value);
    let newLevel = currentLevel + 1;
    let currentMaxHealth = context.system.health.max;
    let newMaxHealth = currentMaxHealth + 1;
    let messageHeader = "Leveling Up to " + newLevel;

    if ( targetXp > currentXp) {
      ui.notifications.warn("Not enough experience!");
      return;
    }

    let roll = new Roll(newLevel+"d8");
    let rollResult = await roll.roll({async:true});
    newMaxHealth = Math.max(newMaxHealth + 1, rollResult._total);
    await context.actor.update({
      'system.attributes.exp.value': Number(currentXp - targetXp),
      'system.attributes.level.value': newLevel,
      'system.health.max': newMaxHealth
    });
    messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">Max HP increased, Increase 3 stats!</span>';
    rollResult.toMessage({
      user: game.user._id,
      speaker: ChatMessage.getSpeaker({ actor: context.actor }),
      flavor: messageHeader
    });

  }

  /**
   * Clear data for the actor
   * @param {Object} context The sheet data.
   * @return {undefined}
   * @private
   */
  async _clearCharacter(context) {
    // If the corresponding game setting is on, clear the inventory
    if (game.settings.get("mouses-knave", "clearInvOnGen")) {

      console.log("Character Generating: Clearing Inventory");
      console.log(context)
      for(let i of context.items)
      {
        let item = this.actor.items.get(i._id);
        item.delete();
      }
      console.log("Character Generating: Resetting stats");
      await context.actor.update({
        'system.health.max': 1,
        'system.health.value': 1,
        'system.attributes.exp.value': 0,
        'system.attributes.level.value': 1,
      })
      console.log("Character Generating: Clearing money");
      await context.actor.update({
        'system.coins.copper': 0,
        'system.coins.silver': 0,
        'system.coins.gold': 0,
        'system.coins.platinum': 0,
      })
      console.log(context.actor)
    }
  }

  /**
   * Generate and set attributes and abilities
   * @param {Object} context The sheet data.
   * @return {undefined}
   * @private
   */
  async _generateStats(context) {

    let act = context.actor

    /* Default picture */
    await act.update({
      'img': "systems/mouses-knave/img/players/knave.jpg"
    });

    /* Generate Movement and Morale */
    console.log("Character Generating: Movement and Morale")
    console.log(act)
    await act.update({'system.movement': 40});
    let moraleRoll = new Roll("5d2");
    await moraleRoll.roll({async:true});
    
    await act.update({'system.attributes.morale.value': moraleRoll.result});

    /* Generate Ability Scores */
    console.log("Character Generating: Ability Scores")
    console.log(act)
    let abilis = duplicate(act.system.abilities);
    for (let [key, ability] of Object.entries(abilis)) {
      let abilityRoll = new Roll("3d6dh2")
      await abilityRoll.roll({async:true})
      console.log("ability roll: "+abilityRoll.result);
      ability.defense = 10 + Number(abilityRoll.result);
    }

    await act.update({'data.abilities': abilis});

    /* Generate Hit points */
    console.log("Character Generating: Rolling Hit Points")
    let HPRoll = new Roll("1d8");
    await HPRoll.roll({async:true});
    let newHealth = {
      value: HPRoll.result,
      min: 0,
      max: HPRoll.result
    }

    await act.update({'data.health': newHealth});
  }

  /**
   * Generate and set the description for the actor based on the characteristic tables
   * @param {Object} context The sheet data.
   * @param {Object} tablePack The generation pack.
   * @return {undefined}
   * @private
   */
  async _generateDescription(context, tablePack) {
    console.log("Character Generating: Description");
    console.log(context.actor);
    
    const characteristicTables = ["Physique","Face","Skin","Hair","Clothing","Virtue","Vice","Speech","Background","Misfortune","Alignment"];
    let characteristicResults = [];
    const packIndex = await tablePack.getIndex();
    for(const characteristic of characteristicTables) {
      const tableId = packIndex.find(e => e.name == characteristic)._id;
      const table = await tablePack.getDocument(tableId);
      const tableDraw = await table.roll({async:true});
      const tableResult = tableDraw.results[0];
      console.log(characteristic+": " + tableResult.text);
      characteristicResults.push(tableResult.text);
    }
    console.log(characteristicResults);
    console.log(characteristicResults[0]);
    let description = "<p>A <i>"+characteristicResults[0]+"</i> person with a <i>"+characteristicResults[1]+"</i> face, <i>"+characteristicResults[2]+"</i> skin, and <i>"+characteristicResults[3]+"</i> hair wearing <i>"+characteristicResults[4]+"</i> clothing.</p>";
    description += "<p><i>"+context.actor.name+"</i> is seen as <i>"+characteristicResults[5]+"</i> but <i>"+characteristicResults[6]+"</i> with a <i>"+characteristicResults[7]+"</i> way of speaking stemming from a history as a <i>"+characteristicResults[8]+"</i>.</p>";
    description += "<p>Being recently <i>"+characteristicResults[9]+"</i> has turned <i>"+context.actor.name+"</i> to work as a <i>"+characteristicResults[10]+"</i> Knave.</p>";
    //apply description
    await context.actor.update({
      'system.notes': description
    });
  }

  /**
  * Generate and set armor, helm, shield, and update defense
  * @param {Object} context The sheet data.
  * @param {Object} tablePack The generation pack.
  * @return {undefined}
  * @private
  */
  async _generateArmor(context, tablePack) {

    let act = context.actor
    const packIndex = await tablePack.getIndex();

    /* Starting Armor */
    const baseArmor = 10
    let newArmorDefense = baseArmor
    console.log("Character Generating: Generating Armor");
    console.log(act)
    const armorTableId = packIndex.find(e => e.name == "Armor")._id;
    const armorTable = await tablePack.getDocument(armorTableId);
    const armorDraw = await armorTable.roll({async:true})
    const armorResult = armorDraw.results[0]
    console.log("Armor: " + armorResult.text)
    console.log(armorResult)
    if (armorResult.text != "No Armor") {
      const armorPack = await game.packs.get("mouses-knave.armor")
      let armorData = await armorPack.getDocument(armorResult.documentId)
      console.log()
      await Item.create(armorData, {parent: act})
      if (armorData.name == "Gambeson") {
        newArmorDefense += 2
      } else if (armorData.name == "Brigandine") {
        newArmorDefense += 3
      } else if (armorData.name == "Chain") {
        newArmorDefense += 4
      }
    } 

    /* Starting Helmet / Shield */
    console.log("Character Generating: Generating Helmet / Shield");
    console.log(act)
    const hsTableId = await packIndex.find(e => e.name == "Helmets and Shields")._id;
    const hsTable = await tablePack.getDocument(hsTableId);
    const hsDraw = await hsTable.roll({async:true})
    const hsResult = hsDraw.results[0]
    console.log("Helm and Shield: " + hsResult.text)
    console.log(hsResult)
    if (hsResult.text != "None") {
      const armorPack = await game.packs.get("mouses-knave.armor");
      const hsPackIndex = await armorPack.getIndex();
      if (hsResult.text.includes("Helmet")) {
        console.log("Helmet!")
        const helm = hsPackIndex.find(e => e.name == "Helmet");
        console.log(helm)
        let helmData = await armorPack.getDocument(helm._id)
        console.log(helmData)
        await Item.create(helmData, {parent: act})
        newArmorDefense++
      }
      if (hsResult.text.includes("Shield")) {
        console.log("Shield!")
        const shield = hsPackIndex.find(e => e.name == "Shield");
        console.log(shield)
        let shieldData = await armorPack.getDocument(shield._id)
        console.log(shieldData)
        await Item.create(shieldData, {parent: act})
        newArmorDefense++
      }
    }
    /* Apply defense bonuses */
    await act.update({
      'system.attributes.armor.defense': newArmorDefense
    })
  }

  /**
  * Generate and set gear
  * @param {Object} context The sheet data.
  * @param {Object} tablePack The generation pack.
  * @return {undefined}
  * @private
  */
  async _generateGear(context, tablePack) {

    let act = context.actor
    const packIndex = await tablePack.getIndex();

    /* All characters start with 2 days of travel rations */
    const toolGearPack = game.packs.get("mouses-knave.tools-and-gear");
    const toolGearIndex = await toolGearPack.getIndex();
    const rationId = toolGearIndex.find(e => e.name == "Travel Rations (1 day)")._id;
    const rationData = await toolGearPack.getDocument(rationId)
    await Item.create(rationData, {parent: act})

    /* Random gold */
    if (game.settings.get("mouses-knave", "randomStartingGold")) {
      console.log("Character Generating: Random Gold")
      let goldRoll = new Roll("5d4");
      await goldRoll.roll({async:true});
      await act.update({
        'data.coins.gold': goldRoll.result
      })
    }

    /* Dungeoneering Gear X2 */
    console.log("Character Generating: Dungeoneering Gear");
    console.log(act)
    const dunGearId = packIndex.find(e => e.name == "Dungeoneering Gear")._id;
    const dunGearTable = await tablePack.getDocument(dunGearId);
    const dunGearPack = game.packs.get("mouses-knave.dungeoneering-gear")
    for (let i = 1; i < 3; i++) {
      const dunGearDraw = await dunGearTable.roll({async:true})
      const dunGearResult = dunGearDraw.results[0]
      console.log("Dungeoneering Gear "+i+": " + dunGearResult.text)
      console.log(dunGearResult)
      const dunGearData = await dunGearPack.getDocument(dunGearResult.documentId)
      console.log(dunGearData)
      await Item.create(dunGearData, {parent: act})
    }

    /* General Gear 1 and 2*/
    for (let i = 1; i < 3; i++) {
      console.log("Character Generating: General Gear "+i);
      console.log(act)
      const ggId = packIndex.find(e => e.name == "General Gear "+i)._id;
      const ggTable = await tablePack.getDocument(ggId);
      const ggPack = game.packs.get("mouses-knave.general-gear-"+i)
      const ggDraw = await ggTable.roll({async:true})
      const ggResult = ggDraw.results[0];
      console.log("General Gear "+i+": " + ggResult.text)
      console.log(ggResult)
      const ggData = await ggPack.getDocument(ggResult.documentId)
      console.log(ggData)
      await Item.create(ggData, {parent: act})
    }
    
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  async _onItemCreate(event) {
    const context = this.getData();

    // Check for encumberance!!
    let makeItemSmall = false;
    if (context.system.attributes.slotsremaining <= 0) {
      ui.notifications.warn("Not enough item slots!");
      return false;
    } else if (context.system.attributes.slotsremaining < 1) {
      makeItemSmall = true;
    }

    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // If the inventory has less than one but greater than 0 slots left, make the item a quarter size
    if (makeItemSmall) itemData.system.slotstaken = 0.25;

    // Finally, create the item!
    return await Item.create(itemData, {parent: context.actor})
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    //console.log(dataset)

    let context = this.getData()
    console.log(context)

    //console.log(this.actor.data.data)

    if (dataset.roll) {
      
      let roll = new Roll(dataset.roll, context.system);
      let messageHeader = dataset.label ? `Rolling ${dataset.label}` : '';
      let r = await roll.roll({async:true})

      if (dataset.label === `morale`) {
        if(r._total > this.object.system.attributes.morale.value)
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critFailure">Is fleeing</span>';
        else
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">Is staying</span>';
      } else if (dataset.label === `initiative`) {
        if(r._total < 4)
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critFailure">Enemies go first</span>';
        else
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">Players go first</span>';
      } else if (dataset.label === `rest`) {
        let newHealth = context.actor.system.health.value += r._total;
        await context.actor.update({'system.health.value': Math.min(newHealth, context.actor.system.health.max)});
        messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">Rest</span>';
      } else if (dataset.label === `fullrest`) {
        await context.actor.update({'system.health.value': context.actor.system.health.max});
        messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">Safe Haven Rest</span>';
      } else if (r.terms[0].faces) {
        let crits = 0
        let fails = 0
        for(let d = 0; d < r.terms[0].number; d++)
        {
          if(r.terms[0].results[d].result === 1)
            fails++
          else if(r.terms[0].results[d].result === r.terms[0].faces)
            crits++
        }
        
        let summary = crits - fails;
        if(summary < 0)
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critFailure">CRITICAL FAILURE!</span>';
        else if(summary > 0)
          messageHeader += ' - <span class="knave-ability-crit knave-ability-critSuccess">CRITICAL SUCCESS!</span>';
      }
      
      r.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: context.actor }),
        flavor: messageHeader
      });
    }
  }

  async _onDrop(event) {
    console.log(event)
    // Try to extract the data
    let dropped;
    try {
      dropped = JSON.parse(event.dataTransfer.getData('text/plain'));
      if (dropped.type !== "Item") return;
    } catch (err) {
      return false;
    }
    
    // Test the item's encumberance against the current amount of slots left open
    let newItem;
    let itemSlotsTaken;
    console.log(dropped)
    // We have to do things a bit different if the item is from a compendium.
    if (dropped.uuid) {
      newItem = await fromUuid(dropped.uuid);
      console.log(newItem)
      itemSlotsTaken = newItem.system.slotstaken;
    // Things also change if we provide data explicitly, such as dragging from another player.
    } else if (dropped.system) {
      itemSlotsTaken = Number(dropped.system.slotstaken);
    } else {
      itemSlotsTaken = game.items.get(dropped.id).system.slotstaken
    }

    let context = this.getData();
    if (context.system.attributes.slotsremaining < itemSlotsTaken) {
      ui.notifications.warn("Not enough item slots!");
      return false;
    }

    // Case 1 - Import from a Compendium pack
    const actor = context.actor;
    if (dropped.uuid) {
      return await Item.create(newItem, {parent: actor})
      //return actor.importItemFromCollection(dropped.pack, dropped.id);
    }
    // Case 2 - Data explicitly provided
    else if (dropped.system) {
      let sameActor = dropped.actorId === actor._id;
      if (sameActor && actor.isToken) sameActor = dropped.tokenId === actor.token.id;
      if (sameActor) return this._onSortItem(event, dropped.data); // Sort existing items
      else return actor.createEmbeddedEntity("OwnedItem", duplicate(dropped.system)); // Create a new Item
    }
    // Case 3 - Import from World entity
    else {
      let item = game.items.get(dropped.id);
      console.log(item)
      if (!item) return;
      //return actor.createEmbeddedEntity("OwnedItem", duplicate(item.data));
      return await Item.create(item, {parent: actor})
    }
  }

}
