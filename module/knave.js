// Import Modules
import { knaveActor } from "./actor/actor.js";
import { knaveActorSheet } from "./actor/actor-sheet.js";
import { knaveMonsterSheet } from "./monster/monster-sheet.js";
import { knaveItem } from "./item/item.js";
import { knaveItemSheet } from "./item/item-sheet.js";

Hooks.once('init', async function() {

  game.knave = {
    knaveActor,
    knaveItem
  };

  /* Register Game Settings */
  game.settings.register("mouses-knave", "autoMonsterHP", {
    name: "Automatically calculate monster HP?",
    hint: "Calculates monster HP based on hit dice and multiplier.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  game.settings.register("mouses-knave", "clearInvOnGen", {
    name: "Clear inventory on character generation?",
    hint: "Delete a character's current inventory before generation.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  game.settings.register("mouses-knave", "coinsHaveWeight", {
    name: "Coins have weight?",
    hint: "100 Coins take up 1 slot",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  game.settings.register("mouses-knave", "randomStartingGold", {
    name: "Start with gold?",
    hint: "Should new characters be generated with some random gold?",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  game.settings.register("mouses-knave", "levelUpExperience", {
    name: "Experience to Level",
    hint: "How much experience is required and spent by leveling up",
    scope: "world",
    config: true,
    default: 1000,
    type: Number
  });

  game.settings.register("mouses-knave", "giveThanks", {
    name: "Give your thanks?",
    hint: "Special thanks to Aidan Habedank!",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });

  

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d20",
    decimals: 2
  };

  // Define custom Entity classes
  CONFIG.Actor.entityClass = knaveActor;
  CONFIG.Item.entityClass = knaveItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("mouses-knave", knaveActorSheet, {types: ["character"], makeDefault: true });
  Actors.registerSheet("mouses-knave", knaveMonsterSheet, {types: ["monster"], makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("mouses-knave", knaveItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function() {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function(str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('toUpperCase', (str) => {
    return str.toUpperCase();
  })
});